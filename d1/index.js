// console.log("Hello World");

//Repition Control Structure (Loops)
	//Loops are one of the most important feature that programming must have
	//It let's us execute code repeatedly in a pre-set number or maybe forever



		//MINI ACTIVITY
			//create a function greeting and invoke it 10 times to print "Hi, Batch 211!"

		function greeting(){
			console.log("Hi, Batch 211!");
		};

		greeting();
		greeting();
		greeting();
		greeting();
		greeting();
		greeting();
		greeting();
		greeting();
		greeting();
		greeting();

		//or we can just use loops

		let countNum = 10;

		while(countNum !==0){
			console.log("This is printed inside the loop: " + countNum);
			greeting();
			countNum--;
		};


	//While Loop
	/*
		A while loop takes in a n expression/condition
		If the condition evaluates to true, the statements inside the code block will be executed

		SYNTAX:
			while(expression/condition){
				statement/code block
				final expression ++/-- (iteration)
			}

		- expressions/conditions - these are the unit of code that are being evaluated in our loop
		- Loop will run while the condition/expression is true

		- statement/code block - codes/instructions that will be executed several times

		- finaExpression - indicates how to advance the loop
	*/

		let count = 5;
		//while the value of count is not equal to 0
		while(count !==0){
			//the current value of count is printed out
			console.log("While: " + count);
			//decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
			count--;
		};

	/*	let num = 20;

		while(num !== 0){
			console.log("While: num " + num);
			num--;
		}*/


		let digit = 5;

		while(digit !==20){
			console.log("While: digit " + digit);
			digit++;
		}


		/*let num1 = 1;

		while(num1 === 2){
			console.log("While: num1 "+ num1);
			num1--
		}*/  //it will not print because from the start num1 is not equal to 2


	//do while loop
	/*
		- A do-while loop works a lot like the while loop
		- But unlike while loops, do-while loops quarantee that the code will be executed at lease once

		SYNTAX:
			do{
				statement/code block
				final/expression ++/--
			}while (expression/condition);


		How the do-while loop works:
		1. The statements in the "do" block executes once
		2. The message "Do While: " + number will be printed out in the console
		3. After executing once, the while statement will evaluate whether to run the next iteration of the loop based on the given expression/condition
		4. If the expression/condition is not true, another iteration of the loop will be executed and will be repeated until the condtion is met
		5. If the condition is true, the loop will stop
	*/

		//Number() method is like parseInt() method
		let number = Number(prompt("Give me a number"));
		do {
			console.log("Do while: " + number);
			number += 1;
		}while(number<10);



	//for Loop
	/*
		- A for loop is more flexible that while and do-while loops

		It consists of three parts:
		1. Initialization value that will track the progression of the loop
		2. Expression/Condition that will be evaluated which will determine if the loop will run one more time
		3. finalExpression indicates how to advance the loop


		SYNTAX:

			for(initialization; expression/condition; finalExpression){
				statement/code of block;
			};
	*/

		for(let count = 0; count <=20; count++){
			console.log("For Loop: " + count);
		};


		/*MINI ACTIVITY

		 Refactor the code above that the loop will only print the even numbers
		 Take a screenshot of your work's console and send it to our batch chat
		 */

		for(let count = 0; count <=20; count++){
			if(count % 2 === 0) {
			//or if(count % 2 === 0 && count !==0)
				console.log("For Loop Even: " + count);
			}
		};


		let myString = "Camille Doroteo";
		//characters in strings may be counted using the .length property
		//strings are special compared to other data types
		console.log(myString.length);


	//Accessing elements of a string
	//Individual characters of a string may be accessed using its index number
	//the first character in a string corresponds to the number 0, the next is 1...

		console.log(myString[2]);
		console.log(myString[0]);
		console.log(myString[8]);
		console.log(myString[14]);


		for(let x=0; x<myString.length; x++){
			console.log(myString[x]);
		};


		//MINI ACTIVITY  - create a variable named myFullName and create a loop
		let myFullName = "Carmina Gordula";
		for(let x=0; x<myFullName.length; x++){
			console.log(myFullName[x]);
		};


		let myName = "NehEmIAh";
		let myNewName = "";

		for(let i=0; i<myName.length; i++){

			if(
				myName[i].toLowerCase() == "a" ||
				myName[i].toLowerCase() == "i" ||
				myName[i].toLowerCase() == "u" ||
				myName[i].toLowerCase() == "e" ||
				myName[i].toLowerCase() == "o" 
			){
				//if the letter in the name is a vowel, it will print the number 3
				console.log(3);
			}
			else {
				//print in the console all non-vowel characters in the name
				console.log(myName[i]);
				myNewName += myName[i];
			}
		}
		console.log(myNewName);
		//concatenate the characters based on the given condition


	//Continue and Break Statements
		/*
			The continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

			The break statement is used to terminate the current loop once a match has been found
		*/

		for(let count=0; count<=20; count++){

			if(count % 2 === 0){
				console.log("Even number");
				//tells the console to continue to the next iteration of the loop
				continue;
				//this ignores all the statements located after the continue statement
				// console.log("Hello");  //it will not print if there's a continue
			}
			console.log("Continue and Break: " + count);

			if(count > 10){
				//break - it tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
				//number values after 10 will no longer be printed
				break;
			}
		};


		let name = "alexandro";

		for(let i = 0; i<name.length; i++){
			//the current letter is printed out based on its index
			console.log(name[i]);

			if(name[i].toLowerCase() === "a"){
				console.log("Continue to the next iteration");
				continue;
			}
			if(name[i].toLowerCase() === "d"){
				break;
			}
		}